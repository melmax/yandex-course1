#include "iostream"
#include "vector"
#include "string"
using namespace std;

bool contains(vector<string> words, string w) {
    for (auto s : words) {
        if (s == w) {
            return true;
        }
    }
    return false;
}

int main() {
    vector<string> knownNames = {"Maxim", "Dima", "Masha", "Vova", "Andrey", "Alexey", "Pavel", "Ilya", "Nataly", "Sasha", "Serega"};

    bool isFirst = true;
    bool isKnown;
    do {
        if (isFirst) {
            cout << "Hello, what is your name?\n";
            isFirst = false;
        } else {
            cout << "So, what is your name?\n";
        }
        string name;
        cin >> name;
        if ((isKnown = contains(knownNames, name))) {
            cout << "very nice, " << name << endl;
        } else {
            cout << "I'm sorry, but I don't know such a name" << endl;
        }
    } while (!isKnown);
    return 0;
}
