#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
    int a, b;
    cin >> a >> b;
//    for (int i = a; i <= b; i++) {
//        if (i % 2 == 0) {
//            cout << i << " ";
//        }
//    }

    int i;
    if (a % 2 == 0) {
        i = a;
    } else {
        i = a + 1;
    }

    while (i <= b) {
        cout << i << " ";

        i += 2;
    }

    return 0;
}
