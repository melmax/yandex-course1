#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
    vector<int> nums = {1, 2, 3, 4, 5};
    int quantity = count(begin(nums), end(nums), 5 );
    cout << "THERE ARE " <<quantity << " fives";
    return 0;
}
