#include <iostream>
using namespace std;

int main()  {
    string s;
    cin >> s;
    int c = 0;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == 'f') {
            c++;
        }
        if (c == 2) {
            cout << i;
            break;
        }
    }
    if (c == 0) {
        cout << -2;
    } else if (c == 1) {
        cout << -1;
    }
    return 0;
}
