#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main() {
    int i;
    int n;
    cin >> n;
    for (i = 1; i <= n; i++) {
        cout << "| " << i << "\t| " << n + 1 - i << "\t|" << endl;
    }
    return 0;
}
