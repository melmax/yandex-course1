#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
    srand((unsigned) time(0));
    int randomNumber = rand() % 100 + 1;
    int attempt = 1;
    cout << "Hello, my friend. Would you like to play a game? I made a random number from 1 to 100. You have 10 attempts to guess." << endl;
    do {
        cout << "Attempt " << attempt << ": ";
        int userNumber;
        cin >> userNumber;
        if (userNumber == randomNumber) {
            cout << "Congratulations! YOU WON!" << endl;
            break;
        } else {
            if (randomNumber > userNumber) {
                cout << "My number is bigger" << endl;
            } else {
                cout << "My number is smaller" << endl;
            }
            if (attempt < 10) {
                cout << "You have " << (10 - attempt) << " attempts to guess" << endl;
            } else {
                cout << "You lose! AHAHAHAHA!" << endl;
                break;
            }
        }
        attempt++;
    } while (true);
    system("pause");
    return 0;
}
