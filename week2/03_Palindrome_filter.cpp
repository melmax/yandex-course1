#include <iostream>
#include <vector>
#include <string>
using namespace std;

bool isPalindrome(string s) {
    bool isPalindrome = true;
    for (int i = 0; i < s.length() / 2; i++) {
        if (s[i] != s[s.length() - i - 1]) {
            isPalindrome = false;
        }
    }
    return isPalindrome;
}

vector<string> palindromeFilter(vector<string> words, int minLength) {
    vector<string> filteredWords = {};
    for (int i = 0; i < words.size(); i++) {
        if (words[i].length() >= minLength && isPalindrome(words[i])) {
            filteredWords.push_back(words[i]);
        }
    }
    return filteredWords;
}

vector<string> inputWords() {
    vector<string> words;
    string s;
    while (cin) {
        getline(cin, s);
        words.push_back(s);
    }
    return words;
}

int main(int argc, char* argv[]) {
    int minLength;
    if (argc > 1) {
        minLength = stoi(argv[1]);
    } else {
        minLength = 0;
    }
    vector<string> words = inputWords();
    vector<string> filteredWords = palindromeFilter(words, minLength);
    for (int i = 0; i < filteredWords.size(); i++) {
        cout << filteredWords[i] << endl;
    }
    return 0;
}
