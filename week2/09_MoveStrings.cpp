//
// Created by max on 09.07.2020.
//
#include <iostream>
#include <vector>
#include <string>
using namespace std;

void MoveStrings(vector<string>& source, vector<string>& destination) {
    for (auto s : source) {
        destination.push_back(s);
    }
    source.clear();
}


int main() {
    vector<string> words = {"hey", "hi"};
    words.clear();
    // теперь вектор words пуст
    vector<string> transport = {"car", "ship", "train"};
    vector<string> b = {"rocket"};
    vector<string> gamemodeCommand  = {"gamemode creative", "gamemode survival", "gamemode adventure"};
    vector<string> d = {"qqwe", "jtyj"};

    MoveStrings(b, transport);
    MoveStrings(gamemodeCommand, transport);
    MoveStrings(d, transport);

    cout << "a:" << endl;
    for (auto s : transport) {
        cout << "\t" << s << endl;
    }

    cout << endl << "b:" << endl;
    for (auto s : b) {
        cout << "\t" <<s << endl;
    }

    cout << endl << "c:" << endl;
    for (auto s : gamemodeCommand) {
        cout << "\t" <<s << endl;
    }

    cout << endl << "d:" << endl;
    for (auto s : d) {
        cout << "\t" <<s << endl;
    }

    return 0;
}
