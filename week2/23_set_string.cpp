//
// Created by max on 24.07.2020.
//
#include <iostream>
#include <set>
#include <string>
using namespace std;

int main() {
    int n;
    cin >> n;
    set<string>strings;
    for (int i = 0; i < n; i++) {
        string s;
        cin >> s;
        strings.insert(s);
    }
    cout << strings.size();
    return 0;
}
