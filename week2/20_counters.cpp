//
// Created by max on 21.07.2020.
//
#include <iostream>
#include <map>
#include <vector>

using namespace std;

void PrintMap(const map<string, int>& m) {
    for (const auto& item :m) {
        cout << item.first << ": " << item.second << endl;
    }
}


int main() {
    vector<string> words = {"one", "two", "three", "three"};
    map<string, int> counters;
    for (const string& word : words) {
        ++counters[word];
    }
    PrintMap(counters);
    return 0;
}
