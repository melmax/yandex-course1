//
// Created by max on 22.07.2020.
//
#include <iostream>
#include <set>

using namespace std;

void PrintSet(const set<string>& s) {
    cout << "size =" <<s.size()  <<endl;
    for (auto x :  s) {
        cout << x << endl;
    }
}

int main() {
    set<string> famous_persons;
    cout << "famous persons" << endl;
    famous_persons.insert("Jens Bergensten");
    famous_persons.insert("Gabe Logan Newell");
    famous_persons.insert("Maxim Melehov Pavlovich");
    PrintSet(famous_persons);

    famous_persons.erase("Maxim Melehov Pavlovich");
    PrintSet(famous_persons);
    return 0;
}
