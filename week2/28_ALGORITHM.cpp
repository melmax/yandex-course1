//
// Created by max on 31.07.2020.
//
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;

int main() {
    cout << "this program will show the largest and smallest row " << endl;
    string s1;
    string s2;
    cin >> s1;
    cin >> s2;
    cout << "________________________________" << endl;
    cout << "largest row " << min(s1, s2) << endl;
    cout << "largest row " << max(s1, s2) << endl;
    cout << "________________________________" << endl;
    return 0;
}
