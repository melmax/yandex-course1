//
// Created by max on 14.07.2020.
//
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> temperatures;
    int n;
    int k = 0;
    int sum = 0;
    cin >> n;
    for (int i = 0; i < n; i++) {
        int temperature;
        cin >> temperature;
        temperatures.push_back(temperature);
        sum += temperature;

    }
    int avg = sum / n;
    for(int i = 0; i < n; i++) {
        if (temperatures[i] > avg) {
            k++;
        }
    }
    cout << k;
    for(int i = 0; i < n; i++) {
        if (temperatures[i] > avg) {
            cout << i << " ";
        }
    }

    return 0;
}
