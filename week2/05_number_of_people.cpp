#include <iostream>
#include <vector>
#include <string>
#include <chrono>

using namespace std;
using namespace std::chrono;

struct Person {
    string name;
    string surname;
    int ago;
};

vector<Person> GetMoscowPopulation();

void PrintPopulationSize(vector<Person> p) {
    cout << "there are " << p.size() <<
    " people in Moscow" << endl;
}

int main () {
    auto start = steady_clock::now();
    vector<Person> people = GetMoscowPopulation();
    auto finish = steady_clock::now();
    cout << "GetMoscowPopulation "
        << duration_cast<milliseconds>(finish - start).count()
        << "ms ";

    start = steady_clock::now();
    PrintPopulationSize(people);
    finish  = steady_clock::now();
    cout << "PrintPopulationSize "
        << duration_cast<milliseconds>(finish - start).count()
        << "ms";
    return 0;
}

vector<Person> GetMoscowPopulation() {
    vector<Person> result;
    for (int i = 0; i < 12500000; i++)
        result.push_back({"Ivan", "Ivanov", 25});
    return result;
}
