//
// Created by max on 20.07.2020.
//

#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct Person {
    bool isWorry;
};

void comeToQueue(vector<Person>& queue, int qty) {
    Person person = { false};
    if (qty >= 0) {
        for (int i = 0; i < qty; i++) {
            queue.push_back(person);
        }
    } else {
//        for (int i = 0; i > qty; i--) {
//            queue.pop_back();
//        }
        if (queue.size() >= -qty) {
            for (int i = 0; i < -qty; i++) {
                queue.pop_back();
            }
        } else {
            cout << "Have only " << queue.size() << " persons" << endl;
        }
    }
}

void setWorry(vector<Person>& queue, int index, bool isWorry) {
    if (index >= 0 && index < queue.size()) {
        queue[index].isWorry = isWorry;
    } else {
        cout << "No person with index " << index << endl;
    }
}

void printWorryCount(vector<Person>& queue) {
    int count = 0;
    for (int i = 0; i < queue.size(); i++) {
        if (queue[i].isWorry) {
            count++;
        }
    }
    cout << count << endl;
}

int main() {
    vector<Person> queue;
    while (true) {
        string command;
        cin >> command;

        if (command == "exit") {
            cout << "Exiting..." << endl;
            break;
        }
        if (command == "come") {
            int qty;
            cin >> qty;
            comeToQueue(queue, qty);
            continue;
        }
        if (command == "worry") {
            int index;
            cin >> index;
            setWorry(queue, index, true);
            continue;
        }
        if (command == "quiet") {
            int index;
            cin >> index;
            setWorry(queue, index, false);
            continue;
        }
        if (command == "worry_count") {
            printWorryCount(queue);
            continue;
        }
        if (command == "dump") {
            if (!queue.empty()) {
                for (int i = 0; i < queue.size(); i++) {
                    cout << i << ") " << (queue[i].isWorry ? "worry" : "quiet") << endl;
                }
            } else {
                cout << "Queue is empty" << endl;
            }
            continue;
        }
        cout << "Unknown command \"" << command << "\"" << endl;
    }
    return 0;
}
