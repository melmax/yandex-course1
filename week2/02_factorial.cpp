#include <iostream>
using namespace std;

unsigned long long factorial(unsigned int n) {
    unsigned long long factorial = 1;
    for (int i = 2; i <= n; i++) {
        factorial *= i;
    }
    return factorial;
}

int main() {
    int n;
    cin >> n;
    cout << factorial(n) << endl;

    return 0;
}
