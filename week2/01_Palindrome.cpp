#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main() {
    string s;
    cout << "this program is looking for a palindrome" << endl;
    cin >> s;

//    for (int i = 0; i < s.length(); i++) {
//        cout << s[i] << ".";
//
//    for (int i = s.length() - 1; i >= 0; i--) {
//        cout << s[i] << " ";
//    }

    bool isPalindrome = true;
    for (int i = 0; i < s.length() / 2; i++) {
        cout << "| " << s[i] << "\t| " << s[s.length() - i - 1] << "\t|" << endl;
        if (s[i] == s[s.length() - i - 1]) {
//            cout << "OK" << endl;
        } else {
            isPalindrome = false;
        }
    }

//    if (isPalindrome) {
//        cout << "true" << endl;
//    } else {
//        cout << "false" << endl;
//    }

//    cout << (isPalindrome ? "true" : "false") << endl;
    cout << "The word \"" << s << "\" " << (isPalindrome ? "is " : "is NOT ") << "palindrome" << endl;
    return 0;
}
