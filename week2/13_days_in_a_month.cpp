//
// Created by max on 14.07.2020.
//
#include <iostream>
#include <vector>
using namespace std;

void PrintVector(const vector<int>& v) {
    for (auto s : v) {
        cout << s << endl;
    }
}

int main() {
    vector<int> days_in_a_months = {31, 28, 31, 30, 31};
    if (true) {               //не смотря на то, что в векторе строго указаны значения к примеру (28) их можно изменять как угодно!
        days_in_a_months[1]++;
    }
    PrintVector(days_in_a_months);

    return 0;
}
