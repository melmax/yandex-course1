//
// Created by max on 19.07.2020.
//

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include "windows.h"

using namespace std;

struct Player {
    string name;
    bool isBot;
    bool isActive;
};

char getLastChar(const string& word) {
    const int wordLength = word.size();
    int i = wordLength - 1;
    while (word[i] == '\xEC' && i > 0) {
        i--;
    }
    return word[i];
}

bool checkWordFirstChar(const string& word, const vector<string>& usedWords) {
    if (!usedWords.empty()) {
        if (word[0] != getLastChar(usedWords[usedWords.size() - 1])) {
            return false;
        }
    }
    return true;
}

bool checkWordUnique(const string& word, const vector<string>& usedWords) {
    if (!usedWords.empty()) {
        for (int i = 0; i < usedWords.size(); i++) {
            if (usedWords[i] == word) {
                return false;
            }
        }
    }
    return true;
}

void printWordWithDelays(const string& word) {
    int length = word.size();
    for (int i = 0; i < length; i++) {
        cout << word[i];
        Sleep(rand() % 295 + 5);
    }
    cout << endl;
}

string getBotWord(const vector<string>& usedWords) {
    char firstChar;
    if (!usedWords.empty()) {
        firstChar = getLastChar(usedWords[usedWords.size() - 1]);
    } else {
        firstChar = '\x00';
    }

    vector<string> dictionary;

    ifstream infile("word_rus.txt");
    string line;
    while (getline(infile, line)) {
        istringstream iss(line);
        if (firstChar != '\x00') {
            if (checkWordFirstChar(line, usedWords) && checkWordUnique(line, usedWords)) {
                dictionary.push_back(line);
            }
        } else {
            dictionary.push_back(line);
        }
    }
    infile.close();

    if (!dictionary.empty()) {
        return dictionary[rand() % dictionary.size()];
    } else {
        return "";
    }
}

int getActivePlayersCount(const vector<Player>& players) {
    int count = 0;
    for (int i = 0; i < players.size(); i++) {
        if (players[i].isActive) {
            count++;
        }
    }
    return count;
}

int getFirstActivePlayerIndex(const vector<Player>& players) {
    for (int i = 0; i < players.size(); i++) {
        if (players[i].isActive) {
            return i;
        }
    }
    return -1;
}

int main() {
    vector<string> usedWords;
    int playersNumber;
    vector<Player> players;
    cout << "Enter number of players: ";
    cin >> playersNumber;
    for (int i = 0; i < playersNumber; i++) {
        cout << "Enter name of player " << (i + 1) << ": ";
        Player player;
        cin >> player.name;
        player.isBot = player.name.find("bot") != string::npos;
        player.isActive = true;
        players.push_back(player);
    }

    srand((unsigned) time(0));
    int currentPlayerIndex = 0;

    while (true) {
        if (getActivePlayersCount(players) == 1) {
            int firstActivePlayerIndex = getFirstActivePlayerIndex(players);
            if (firstActivePlayerIndex >= 0) {
                cout << endl << "Player " << players[firstActivePlayerIndex].name << " WON!" << endl;
                break;
            } else {
                cout << "Impossible situation" << endl;
            }
        }

        if (!players[currentPlayerIndex].isActive) {
            currentPlayerIndex = (currentPlayerIndex + 1) % playersNumber;
            continue;
        }

        cout << endl << usedWords.size() + 1 << ") " << players[currentPlayerIndex].name << ", ";
        if (usedWords.empty()) {
            cout << "enter any word: ";
        } else {
            cout << "enter word starting from \"" << getLastChar(usedWords[usedWords.size() - 1]) << "\": ";
        }
        string word;

        if (players[currentPlayerIndex].isBot) {
            Sleep(1500);
            word = getBotWord(usedWords);
            if (word == "") {
                players[currentPlayerIndex].isActive = false;
                cout << endl << "Player " << players[currentPlayerIndex].name << " has lost and left the game" << endl;
                continue;
            }
            printWordWithDelays(word);
        } else {
            cin >> word;
        }

        if (word == "/words") {
            if (!usedWords.empty()) {
                cout << "Previously used words:" << endl;
                for (int i = 0; i < usedWords.size(); i++) {
                    cout << "\t" << usedWords[i] << endl;
                }
            } else {
                cout << "There's no previously used words" << endl;
            }
            continue;
        }

        if (word == "/back") {
            if (!usedWords.empty()) {
                usedWords.pop_back();
                currentPlayerIndex = (currentPlayerIndex - 1 + playersNumber) % playersNumber;
            } else {
                cout << "Can't get back" << endl;
            }
            continue;
        }

        if (word == "/give_up") {
            cout << "Player " << players[(currentPlayerIndex - 1 + playersNumber) % playersNumber].name << " gave up!" << endl;
            players[currentPlayerIndex].isActive = false;
        }

        if (word == "/exit") {
            cout << "Exiting..." << endl;
            break;
        }

        bool isGoodWord = true;
        if (!checkWordFirstChar(word, usedWords)) {
            cout << "First letter of word doesn't match last letter of previous word" << endl;
            isGoodWord = false;
        }
        if (!checkWordUnique(word, usedWords)) {
            cout << "This word already used" << endl;
            isGoodWord = false;
        }
        if (isGoodWord) {
            usedWords.push_back(word);
            currentPlayerIndex = (currentPlayerIndex + 1) % playersNumber;
        }
    }

    return 0;
}
