//
// Created by max on 09.07.2020.
//

#include <iostream>

using namespace std;

int main() {
    int result;
    cin >> result;
    do {
        char operation;
        cin >> operation;
        if (operation == '=') {
            cout << result << endl;
            break;
        }
        int operand;
        cin >> operand;
        switch (operation) {
            case '+' :
                result += operand;
                break;
            case '-' :
                result -= operand;
                break;
            case '*' :
                result *= operand;
                break;
            case '/' :
                result /= operand;
                break;
            default :
                cout << "Unknown operator" << endl;
        }
    } while (true);

    return 0;
}
