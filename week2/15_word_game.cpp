//
// Created by max on 19.07.2020.
//

#include <iostream>
#include <vector>
using namespace std;

char getLastChar(const string& word) {
    const int wordLength = word.size();
    int i = wordLength - 1;
    while (word[i] == '\xEC' && i > 0) {
        i--;
    }
    return word[i];
}

bool checkWordFirstChar(const string& word, const vector<string>& usedWords) {
    if (!usedWords.empty()) {
        if (word[0] != getLastChar(usedWords[usedWords.size() - 1])) {
            return false;
        }
    }
    return true;
}
bool checkWordUnique(const string& word, const vector<string>& usedWords) {
    if (!usedWords.empty()) {
        for (int i = 0; i < usedWords.size(); i++) {
            if (usedWords[i] == word) {
                return false;
            }
        }
    }
    return true;
}

int main() {
    vector<string> usedWords;
    int playersNumber;
    vector<string> players;
    cout << "Enter number of players: ";
    cin >> playersNumber;
    for (int i = 0; i < playersNumber; i++) {
        cout << "Enter name of player " << (i + 1) << ": ";
        string playerName;
        cin >> playerName;
        players.push_back(playerName);
    }

    int currentPlayerIndex = 0;

    while (true) {
        cout << endl << usedWords.size() + 1 << ") " << players[currentPlayerIndex] << ", ";
        if (usedWords.empty()) {
            cout << "enter any word: ";
        } else {
            cout << "enter word starting from \"" << getLastChar(usedWords[usedWords.size() - 1]) << "\": ";
        }
        string word;
        cin >> word;

        if (word == "/words") {
            if (!usedWords.empty()) {
                cout << "Previously used words:" << endl;
                for (int i = 0; i < usedWords.size(); i++) {
                    cout << "\t" << usedWords[i] << endl;
                }
            } else {
                cout << "There's no previously used words" << endl;
            }
            continue;
        }

        if (word == "/back") {
            if (!usedWords.empty()) {
                usedWords.pop_back();
                currentPlayerIndex = (currentPlayerIndex - 1 + playersNumber) % playersNumber;
            } else {
                cout << "Can't get back" << endl;
            }
            continue;
        }

        if (word == "/exit") {
            cout << "Player " << players[(currentPlayerIndex - 1 + playersNumber) % playersNumber] << " won!" << endl;
            break;
        }

        bool isGoodWord = true;
        if (!checkWordFirstChar(word, usedWords)) {
            cout << "First letter of word doesn't match last letter of previous word" << endl;
            isGoodWord = false;
        }
        if (!checkWordUnique(word, usedWords)) {
            cout << "This word already used" << endl;
            isGoodWord = false;
        }
        if (isGoodWord) {
            usedWords.push_back(word);
            currentPlayerIndex = (currentPlayerIndex + 1) % playersNumber;
        }
    }

    return 0;
}
