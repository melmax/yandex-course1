//
// Created by max on 13.07.2020.
//
#include <iostream>
#include <vector>
using namespace std;

int GetVectorMaxLength(const vector<string>& v) {
    int colSize = 0;
    for (string s : v) {
        if (s.length() > colSize) {
            colSize = s.length();
        }
    }
    return colSize;
}

void PrintHorLine(int colSize, int type) {
    cout << (type == 0 ? "\xDA" : "\xC0");
    for (int i = 0; i < colSize; i++) {
        cout << "\xC4";
    }
    cout << (type == 0 ? "\xBF" : "\xD9") << endl;
}

void PrintVector(const vector<string>& v) {
    int colSize = GetVectorMaxLength(v);

    PrintHorLine(colSize, 0);

    for (string s : v) {
        cout << "\xB3";
        cout << s;
        for (int i = 0; i < colSize - s.length(); i++) {
            cout << " ";
        }
        cout << "\xB3" << endl;
    }

    PrintHorLine(colSize, 1);
}

int main() {
    int n;
    cout << "this is a test program about vectors" << endl;
    cout << "enter the number of words and the words themselves" << endl;
     cin >> n;
//    vector<string> v(n);
//    for (string& s : v) {
//        cin >> s;
//    }

    vector<string> v;
    int i = 0;
    while (i < n) {
        string s;
        cin >> s;
        v.push_back(s);
        cout << "current size = " << v.size() << endl;
        ++i;
    }
    PrintVector(v);

    return 0;
}
