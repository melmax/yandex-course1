//
// Created by max on 10.07.2020.
//

#include <iostream>
#include <vector>
#include <string>
using namespace std;

void reverse(vector<int>& v) {
    int tmp;
    for (int i = 0; i <= v.size() / 2; i++) {
//        cout << v[i];
//        cout << v[v.size() - i - 1];
        tmp = v[i];
        v[i] = v[v.size() - i - 1];
        v[v.size() - i - 1] = tmp;
    }
}

int main() {
    vector<int> numbers = {1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
        60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88,
        89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100};

    cout << "numbers before:" << endl;
    for (auto n : numbers) {
        cout << n << ", ";
    }
    cout << endl;

    reverse(numbers);

    cout << "numbers after:" << endl;
    for (auto n : numbers) {
        cout << n << ", ";
    }
    cout << endl;

    return 0;
}
