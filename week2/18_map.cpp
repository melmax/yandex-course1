//
// Created by max on 20.07.2020.
//

#include <iostream>
#include <map>

using namespace std;

void PrintMap(const map<int, string>& m) {
    cout << "size = " << m.size() << endl;
    for (auto item : m) {
        cout << item.first << ": " << item.second << endl;
    }
}

void PrintReversedMap(const map<string, int>& m) {
    cout << "size = " << m.size() << endl;
    for (auto item : m) {
        cout << item.first << ": " << item.second << endl;
    }
}

map<string, int> BuildReversedMap(const map<int, string>& m) {
    map<string, int> result;
    for (auto item : m) {
        result[item.second] = item.first;
    }
    return result;
}
int main() {
    map<int, string> events;
    events[2009] = "game officially released MINECRAFT";
    events[2017] = "game officially released PREY";
    events[2012] = "game officially released CS:GO";
    PrintMap(events);

    //если написать events.erase(******);
    //              PrintMap(events);
    //то, что было в скобочках events.erase(******) будет удалено

    PrintReversedMap(BuildReversedMap(events));

    return 0;
}
