//
// Created by max on 30.07.2020.
//
#include <iostream>
#include <set>
#include <string>
#include <map>
using namespace std;

bool CompareRoutes(const set<string>& route1, const set<string>& route2) {
    for (const auto & it : route1) {
        if (route2.count(it) == 0) {
            return false;
        }
    }
    for (const auto & it : route2) {
        if (route1.count(it) == 0) {
            return false;
        }
    }
    return true;
}

int GetRouteNumber(const set<string>& route, const map<int, set<string>>& routes) {
    for (const auto & x : routes) {
        if (CompareRoutes(route, x.second)) {
            return x.first;
        }
    }
    return 0;
}

int main() {
    int q;
    cin >> q;
    map<int, set<string>> routes;
    int lastRouteNumber = 0;

    for (int i = 0; i < q; i++) {
        int n;
        cin >> n;

        set<string> r;
        for (int k = 0; k < n; k++) {
            string stop;
            cin >> stop;
            r.insert(stop);
        }

        int routeNumber = GetRouteNumber(r, routes);
        if (routeNumber == 0) {
            lastRouteNumber++;
            routes[lastRouteNumber] = r;
            cout << "New bus " << lastRouteNumber << endl;
        } else {
            cout << "Already exists for " << routeNumber << endl;
        }
    }
    return 0;
}
