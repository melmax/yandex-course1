//
// Created by max on 31.07.2020.
//
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iomanip>
using namespace std;

void PrintVector(const vector<int>& v, const string& title) {
    cout << title << ": ";
    for (auto i : v) {
        cout << i << ' ';
    }
}

int main() {
    vector<int> v {
        1, 3, 2, 5, 4
    };
    cout << "--------------------" <<endl;
    PrintVector(v, "init");
    sort(begin(v), end(v));
    cout << endl;
    cout << "--------------------" <<endl;
    PrintVector(v, "sort");
    cout << endl;
    cout << "--------------------" <<endl;

    return 0;
}
