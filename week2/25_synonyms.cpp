//
// Created by max on 26.07.2020.
//
#include <iostream>
#include <map>
#include <set>
using namespace std;

void add(string word1, string word2) {

}

int main() {
    map<string, set<string>> synonyms1;
    map<string, set<string>> synonyms2;
    while (true) {
        string command;
        cin >> command;

        if (command == "exit") {
            cout << "Exiting..." << endl;
            break;
        }
        if (command == "add") {
            string word1;
            string word2;
            cin >> word1;
            cin >> word2;
            synonyms1[word1].insert(word2);
            synonyms2[word2].insert(word1);
            continue;
        }
        if (command == "check") {
            string word1;
            string word2;
            cin >> word1;
            cin >> word2;
            if (synonyms1[word1].count(word2) || synonyms2[word1].count(word2)) {
                cout << "YES" << endl;
            } else {
                cout << "NO" << endl;
            }
        }
        if (command == "count") {
            string word1;
            cin >> word1;
            cout << synonyms1[word1].size() + synonyms2[word1].size() << endl;
        }
    }
    return 0;
}
