//
// Created by max on 21.07.2020.
//
#include <iostream>
#include <map>

using namespace std;

void PrintMap(const map<string, int>& m) {
    for (const auto& item :m) {
        cout << item.first << ": " << item.second << endl;
    }
}

int main() {
    map<string, int> m = {{"one", 1}, {"two", 2}, {"three", 3}}; //вместо "one","two","three"можно написать что угодно,
                                                                                  // это будет от сортировано по алфавиту...
    //PrintMap(m);
    m.erase("three");
    PrintMap(m); //PrintMap нужно вводить ещё раз потому что мы изменяем "map"
    return 0;
}
