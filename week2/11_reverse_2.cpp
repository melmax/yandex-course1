//
// Created by max on 13.07.2020.
//
#include <iostream>
#include <vector>
using namespace std;

vector<int> reversed(const vector<int>& input) {
    vector<int> result;

    for(int i = input.size() - 1; i >= 0; i--) {
        result.push_back(input[i]);
    }
    return result;
}

int main() {
    vector<int> numbers = {1, 5, 3, 4, 2};
    vector<int> numbers2;
    cout << "numbers before:" << endl;
    for(auto n : numbers) {
        cout << n << ", ";
    }
    cout << endl;

    numbers2 = reversed(numbers);

    cout << "numbers after:" << endl;
    for (auto n : numbers2) {
        cout << n << ", ";
    }
    cout << endl;

    return 0;
}
